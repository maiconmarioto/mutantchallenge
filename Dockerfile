FROM node:12.18.2-alpine3.12

WORKDIR /usr/src/app

COPY package*.json ./
COPY yarn.lock ./

RUN yarn
RUN yarn global add pm2

COPY . ./

RUN yarn build

EXPOSE 8080

CMD ["pm2-runtime", "dist/server.js"]
