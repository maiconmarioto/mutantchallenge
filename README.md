# Desafio Mutant!

## Descrição
>Sua tarefa é fazer um aplicativo que carregue a saida da URL [jsonplaceholder](https://jsonplaceholder.typicode.com/users), que retorna uma lista de usuário em JSON.
  Faça um programa que carregue a saída dessa URL e mostre os seguintes dados:

 1. Os websites de todos os usuários
 2. O Nome, email e a empresa em que trabalha (em ordem alfabética).
 3. Mostrar todos os usuários que no endereço contem a palavra ```suite```
 4. Salvar logs de todas interações no elasticsearch
 5. EXTRA: Criar test unitário para validar os itens a cima.
 	
## Ambiente
>Sua configuração deve ser capaz de ser executada como abaixo:
	1. Implantar uma máquina virtual. Para não gerar custos para você e para nós, use o `vagrant` com
	VirtualBox para provisionar sua instância.
	2. Entregar o aplicativo usando o Docker e garantir que ele &quot;sobreviva&quot; nas reinicializações


## Executando o projeto

Para executar com vagrant, execute o código abaixo na pasta raiz do projeto:

    vagrant up

Não tem vagrant?

    docker-compose up --build -d 

Para ver a aplicação, acesse: http://localhost:8080
Para ver os logs do elasticsearch: http://localhost:9200/users/_search/

## Testes
Para executar os testes, use o seguinte comando na pasta raiz

    yarn jest

## Endpoints
> Além de exibir os dados no frontend, há também os seguintes endpoints:

Todos os dados do usuário: http://localhost:8080/users
Apenas os websites: http://localhost:8080/users/websites
Usuários ordenados: http://localhost:8080/users/ordered-by-name
Usuários em suites: http://localhost:8080/users/on-suite

## Tecnologias 

 - NodeJS
 - Typescript
 - Express
 - Axios
 - Babel
 - Eslint
 - Jest