import { Router } from 'express';
import UserController from './controllers/UserController';

const router = Router();
const userController = new UserController();

router
  .get('/', userController.index)
  .get('/websites', userController.websites)
  .get('/ordered-by-name', userController.orderedUsers)
  .get('/on-suite', userController.usersOnSuite);

export default router;
