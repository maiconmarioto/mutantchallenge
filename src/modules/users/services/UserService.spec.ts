import UserService from './UserService';

describe('UserService', () => {
  const userService = new UserService();

  const users = [
    {
      id: 1,
      name: 'Leanne Graham',
      username: 'Bret',
      email: 'Sincere@april.biz',
      address: {
        street: 'Kulas Light',
        suite: 'Apt. 556',
        city: 'Gwenborough',
        zipcode: '92998-3874',
        geo: { lat: '-37.3159', lng: '81.1496' },
      },
      phone: '1-770-736-8031 x56442',
      website: 'hildegard.org',
      company: {
        name: 'Romaguera-Crona',
        catchPhrase: 'Multi-layered client-server neural-net',
        bs: 'harness real-time e-markets',
      },
    },
    {
      id: 7,
      name: 'Kurtis Weissnat',
      username: 'Elwyn.Skiles',
      email: 'Telly.Hoeger@billy.biz',
      address: {
        street: 'Rex Trail',
        suite: 'Suite 280',
        city: 'Howemouth',
        zipcode: '58804-1099',
        geo: { lat: '24.8918', lng: '21.8984' },
      },
      phone: '210.067.6132',
      website: 'elvis.io',
      company: {
        name: 'Johns Group',
        catchPhrase: 'Configurable multimedia task-force',
        bs: 'generate enterprise e-tailers',
      },
    },
    {
      id: 2,
      name: 'Nicholas Runolfsdottir V',
      username: 'Maxime_Nienow',
      email: 'Sherwood@rosamond.me',
      address: {
        street: 'Ellsworth Summit',
        suite: 'Suite 729',
        city: 'Aliyaview',
        zipcode: '45169',
        geo: { lat: '-14.3990', lng: '-120.7677' },
      },
      phone: '586.493.6943 x140',
      website: 'jacynthe.com',
      company: {
        name: 'Abernathy Group',
        catchPhrase: 'Implemented secondary concept',
        bs: 'e-enable extensible e-tailers',
      },
    },
  ];

  it('should be get a list of users', async () => {
    const data = await userService.get();
    expect(data[0]).toHaveProperty('id');
  });

  it('should return a list of websites', () => {
    const websites = userService.getWebsites(users);
    expect(websites).not.toBe([]);
  });

  it('should return a list users ordered by name', () => {
    const orderedList = userService.getOrderedByName(users);
    expect(orderedList[0].name).toBe('Kurtis Weissnat');
    expect(orderedList[1].name).toBe('Leanne Graham');
  });

  it('should return a list users living on a suite', () => {
    const orderedList = userService.getUsersOnSuite(users);
    expect(orderedList[0]).toBe('Kurtis Weissnat');
  });
});
