import api from '@config/api';
import logger from '@config/logger';
import { User, UserInfo } from '../entities/user';

class UserService {
  async get(): Promise<User[] | null> {
    try {
      const response = await api.get('/users');
      return response.data;
    } catch (e) {
      logger.error(e.message);
      return null;
    }
  }

  getWebsites(users: User[]): string[] {
    return users.map(user => user.website);
  }

  getOrderedByName(users: User[]): UserInfo[] {
    return users
      .sort((a, b) => (a.name < b.name ? -1 : 1))
      .map(user => ({
        name: user.name,
        email: user.email,
        company: user.company.name,
      }));
  }

  getUsersOnSuite(users: User[]): string[] {
    return users
      .filter(user => user.address.suite.toLowerCase().includes('suite'))
      .map(user => user.name);
  }
}

export default UserService;
