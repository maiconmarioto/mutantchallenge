import { Response, Request } from 'express';
import { store } from '@config/elasticSearch';
import UserService from '../services/UserService';

class UserController {
  async index(request: Request, response: Response): Promise<Response> {
    const userService = new UserService();

    const users = await userService.get();

    const websites = userService.getWebsites(users);
    const orderedUsers = userService.getOrderedByName(users);
    const usersOnSuite = userService.getUsersOnSuite(users);

    const result = {
      websites,
      usersOnSuite,
      orderedUsers,
    };

    store(result);

    return response.json({
      users: result,
    });
  }

  async websites(request: Request, response: Response): Promise<Response> {
    const userService = new UserService();
    const users = await userService.get();
    const websites = userService.getWebsites(users);
    store({ websites });
    return response.json({ websites });
  }

  async orderedUsers(request: Request, response: Response): Promise<Response> {
    const userService = new UserService();
    const users = await userService.get();
    const orderedUsers = userService.getOrderedByName(users);
    store({ orderedUsers });
    return response.json({ users: orderedUsers });
  }

  async usersOnSuite(request: Request, response: Response): Promise<Response> {
    const userService = new UserService();
    const users = await userService.get();
    const usersOnSuite = userService.getUsersOnSuite(users);
    store({ usersOnSuite });
    return response.json({ users: usersOnSuite });
  }
}

export default UserController;
