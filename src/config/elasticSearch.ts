import { Client } from '@elastic/elasticsearch';
import logger from '@config/logger';

const client = new Client({
  node: process.env.ELASTIC_URL || 'http://localhost:9200',
});

export async function store(data): Promise<void> {
  await client.index({
    index: 'users',
    body: data,
  });

  logger.info('saved on elasticsearch');
}

// export const elasticSearchConfig = async (): Promise<void> => {
//   try {
//     const url = ;
//     await axios.put(`${url}/logs/users`);
//   } catch (e) {
//     if (!e.message.includes(400)) {
//       logger.info('eu');
//       logger.error(e.message);
//     }
//   }
// };

// export const store = async (item): Promise<void> => {
//   try {
//     const url = process.env.ELASTIC_URL || 'http://localhost:9200';
//     await axios.post(`${url}/users`, { item });
//   } catch (e) {
//     logger.error(e.message);
//   }
// };
