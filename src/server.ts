import path from 'path';
import express, { Express } from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';

import logger, { stream } from '@config/logger';
import routes from './routes';

class Server {
  private app: Express;

  constructor() {
    this.app = express();
  }

  middlewares(app: Express): void {
    app.use(cors());
    app.use(helmet());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(
      morgan(':method :url :status :response-time ms combined', {
        stream,
      }),
    );
  }

  process(): void {
    process.on('uncaughtException', (e: Error) => {
      logger.error({ message: e.message, stack: e.stack });
      process.exit(1);
    });

    process.on('unhandledRejection', (e: Error) => {
      logger.error({ message: e.message, stack: e.stack });
      process.exit(1);
    });
  }

  routes(app: Express): void {
    const publicpath = path.join(__dirname, '../', 'public');
    app.use('/', express.static(publicpath));
    app.use(routes);
  }

  start(): void {
    this.process();
    this.middlewares(this.app);
    this.routes(this.app);

    const { PORT = 8080 } = process.env;

    this.app.listen(PORT, () => {
      return logger.info(`App running on port: ${PORT}`);
    });
  }
}

const server = new Server();
server.start();
