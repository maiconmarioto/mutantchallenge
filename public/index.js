/* eslint-disable */

const { createElement, useState, useEffect } = React;
const { render } = ReactDOM;
const html = htm.bind(createElement);

function App() {
  const [users, setUsers] = useState([]);


  useEffect(() => {
    async function loadData() {
      const response = await axios.get('users');
      const { users } = response.data;
      setUsers(users)
    }
    loadData();
  }, [])

  if (users.length == 0) {
    return null;
  }
  
  const ordered = users.orderedUsers.map(user => (
    html`
      <div className="content">
        <p>${user.name}</p>
        <p>${user.email}</p>
        <p>${user.company}</p>
      </div>
    `
  ))

  const websites = users.websites.map(website => (
    html`
      <div className="link">
          <a target="blank" href="${website}">
            ${website}
            <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
          </a>
      </div>
    `
  ))

  const onSuite = users.usersOnSuite.map(user => (
    html`
    <div className="content">
      <p>${user}</p>
    </div>
    `
  ))

  return html`
    <div className="container">
      <div className="cards">
          <div className="card-container">
              <div className="card">
                  <div className="card-text">
                      <h3>Users:</h3>
                      ${ordered}
                  </div>
              </div>
          </div>

          <div className="card-container">
            <div className="card">
              <div className="card-text">
                <h3>Webistes</h3>
                ${websites}
              </div>
            </div>
          </div>

          <div className="card-container">
              <div className="card">
                <div className="card-text">
                  <h3>Living on a suite</h3>
                  ${onSuite}
                </div>
              </div>
          </div>
      </div>
    </div>
  `;
}

render(html`<${App} />`, document.getElementById('root'));
